var express = require('express');
var app = express();
const PORT = 8082;

app.use('/resume', express.static(__dirname));

app.use('/', function (req, res) {
  res.send('Go to /resume!');
});

app.listen(PORT, function () {
  console.log(`Server up on localhost:${PORT}`);
});
