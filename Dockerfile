FROM node:6-alpine

COPY index.html ./
COPY index.js ./
COPY package*.json ./
RUN npm install

CMD ["node", "index.js"]
EXPOSE $PORT